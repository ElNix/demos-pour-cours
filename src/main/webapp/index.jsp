<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page isELIgnored="false" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<body>
	<h2>Index !</h2>
	<div>
		Statut connexion : <br />
		<c:choose>
			<c:when test="${sessionScope.pseudo!=null}">
				<c:out value="Connect� avec : ${sessionScope.pseudo}" />
			</c:when>
			<c:otherwise>Non Connect�</c:otherwise>
		</c:choose>
	</div>
	<p>
		<a href="CreationVariableSession">Servlet de cr�ation de session</a>
	</p>
	<p>
		<a href="Connexion">Connexion</a>
	</p>
	<p>
		<a href="DestructionSession">D�connexion</a>
	</p>
	<!--  <p>
		<a href="backoffice/indexBackOffice.jsp">Acc�s page restreinte</a>
	</p>-->
</body>
</html>
