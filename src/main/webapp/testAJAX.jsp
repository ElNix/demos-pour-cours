<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<link rel="stylesheet" href="CSS/additionner.css">
</head>
<body>
	<div id="bloc">
		<div>
			<input type="text" id="int1" /> + <input type="text" id="int2" />
		</div>
		<br />
		<div>
			<button style="align: center" id="but">Additionner</button>
		</div>
		<br /> <span id="resultat">Resultat</span>
	</div>

	<script
		src="https://ajax.aspnetcdn.com/ajax/jQuery/jquery-3.3.1.min.js"></script>
	<script>
		$("#but").on("click", function() {

			var int1 = $("#int1").val();
			var int2 = $("#int2").val();

			$.get("Addition?nb1=" + int1 + "&nb2=" + int2, function(data) {
				$("#resultat").html(data);
			});

			console.log($("#resultat").html());
		})
	</script>

</body>
</html>