	<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
		pageEncoding="ISO-8859-1"%>
	<%@taglib  uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
	<%@ page isELIgnored="false" %>
	<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
	<html>
	<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>Affichage Bean ELs</title>
	</head>
	<body>
		<%
			//Pas besoin de récupération avec les EL !
		%>
		<p>Voici la description de mon canap&eacute; !</p>
		<div>
			Mon canap&eacute; a <c:out value="${canapeSalon.nbPlaces}" /> places. <br /> 
			Il est <c:out value="${canapeSalon.couleur}" />
			avec un magnifique motif <c:out value="${canapeSalon.motif}" />
		</div>
		<div>
			<p>Mais je vais le laisser se pr&eacute;senter seul : </p>
			<c:out value="${canapeSalon.descriptionCanape()}" />
		</div>
	</body>
	</html>

