	
	
	<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
		pageEncoding="ISO-8859-1"%>
	<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
	<html>
	<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>Page d'affichage</title>
	</head>
	<body>
		<div>Cette page affiche des donn&eacute;es envoy&eacute;es par
			une Servlet !</div>
		<div>
			Voici un texte :
			<% 
	            String variable1 = (String) request.getAttribute("texte");
	            out.println( variable1 );
	            %>
		</div>
		<div>
			Voici un nombre :
			<% 
				int variable2 = (Integer) request.getAttribute("nombre");
	            out.println( variable2 );
	            %>
	
		</div>
	</body>
	</html>