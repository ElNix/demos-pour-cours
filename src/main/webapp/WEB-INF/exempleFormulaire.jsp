<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>

	<form action="/ma-page-de-traitement" method="post">
	    <div>
	        <label for="name">Nom :</label>
	        <input type="text" id="name" name="user_name" />
	    </div>
	    <div>
	        <label for="mail">e-mail :</label>
	        <input type="text" id="mail" name="user_mail" />
	    </div>
	    <div>
	        <label for="msg">Message :</label>
	        <textarea id="msg" name="user_message">
	        </textarea>
	    </div>
	</form>

</html>