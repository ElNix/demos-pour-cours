	
	
	<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
		pageEncoding="ISO-8859-1"%>
	<%@taglib  uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
	<%@ page isELIgnored="false" %>
		
	<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
	<html>
	<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>Page d'affichage EL</title>
	</head>
	<body>
		<div>Cette page affiche des donn&eacute;es envoy&eacute;es par
			une Servlet !</div>
		<div>
			Voici un texte :
			<c:out value="${texte}" />
		</div>
		<div>
			Voici un nombre :
			<c:out value="${nombre}" />
		</div>
	</body>
	</html>