<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page isELIgnored="false"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Lecture variable session</title>
</head>
<body>
	<div>
	
		<p>
			Affichage de la variable de session exemple : <br /><br /> 
		</p>
		<div>
			Version classique : <br /><br /> 
			<%
				String ex = (String) session.getAttribute("exemple");
				out.println(ex);
			%>
		</div>
		<br /><br />
		<div>
			Version EL : <br /><br /> 
			<c:out value="${sessionScope.exemple}" />
		</div>
		
		<br /> 
		<p>
			<a href="DestructionSession">Cliquez ici pour d�truire la session</a>
		</p>
		<p>
			<a href="CreationVariableSession">Lien vers le servlet de
				cr�ation</a>
		</p>
	</div>
</body>
</html>