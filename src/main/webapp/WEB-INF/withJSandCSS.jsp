<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page isELIgnored="false"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

	<html>
		<head>
			<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
			<title>JS and CSS</title>
			<!--  Appel fichier CSS --><link type="text/css" rel="stylesheet" href="CSS/test.css" />
		</head>
		<body>
			<h1>Gros titre</h1>
			<p id="compteur">
				<c:out value="${compteur}" />
			</p>
			<!-- Appel fichier JavaScript --><script src="JS/test.js"></script>
		</body>
	</html>


