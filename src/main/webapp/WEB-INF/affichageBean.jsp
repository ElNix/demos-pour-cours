	<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
		pageEncoding="ISO-8859-1"%>
	<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
	<html>
	<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>Affichage Bean</title>
	</head>
	<body>
		<%
			//Récupération du bean
			beans.Canape can = (beans.Canape) request.getAttribute("canapeSalon");
		%>
		<p>Voici la description de mon canap&eacute; !</p>
		<div>
			Mon canap&eacute; a <% out.println(can.getNbPlaces()); %> places. <br /> 
			Il est <% out.println(can.getCouleur()); %>
			avec un magnifique motif <% out.println(can.getMotif()); %>
		</div>
		<div>
			<p>Mais je vais le laisser se pr&eacute;senter seul : </p>
			<%
				out.println(can.descriptionCanape());
			%>
		</div>
	</body>
	</html>

