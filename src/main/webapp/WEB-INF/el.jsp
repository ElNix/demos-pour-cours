
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page isELIgnored="false"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Tests EL</title>
</head>
<body>

	<div>

		Affichage valeur simple :
		<c:out value="${valeur}" />
		<br /> Affichage valeur calcul�e :
		<c:out value="${valeur > 10}" />
		<br /> Affichage code avec protection :
		<c:out value="${code}" />
		<br /> Affichage code sans protection :
		<c:out value="${code}" escapeXml="false" />
	</div>


	<br />
	<br />

	<div>
		<c:if test="${ valeur > 10 }">
    		Valeur est sup�rieur � 10.
		</c:if>
		<br />
		<c:if test="${ valeur > 100 }">
    		Valeur est sup�rieur � 100.
		</c:if>
	</div>

	<br />
	<br />
	<br />

	<div>
		<c:choose>
			<c:when test="${meteo.equals('Soleil')}">Il fait beau</c:when>
			<c:when test="${meteo.equals('Nuages')}">Il fait assez beau</c:when>
			<c:when test="${meteo.equals('Pluie')}">Il fait moche</c:when>
			<c:otherwise>ERREUR DE SAISIE</c:otherwise>
		</c:choose>
	</div>

	<br />
	<br />
	<br />
	
	<div>
		<c:forEach var="i" begin="0" end="10" step="1">
			<c:out value="${i} " />
		</c:forEach>
	</div>
	<br />
	<div>
		<c:forEach items="${liste}" var="canape">
			<c:out value="Canap : ${canape.nbPlaces} et motif : ${canape.motif}" />
			<br />
		</c:forEach>
	</div>
	
</body>
</html>