package filters;

import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet Filter implementation class BackOffice
 */

	import javax.servlet.annotation.WebFilter;

	@WebFilter( urlPatterns = "/backoffice/*" )
	public class BackOffice implements Filter {

	/**
	 * Default constructor. 
	 */
	public BackOffice() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see Filter#destroy()
	 */
	public void destroy() {
		// TODO Auto-generated method stub
	}

	/**
	 * @see Filter#doFilter(ServletRequest, ServletResponse, FilterChain)
	 */
	
	public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain) throws IOException, ServletException {
        //Conversion des ServletRequest et ServletResponse
		//En HttpServletRequest et HttpServletResponse
		//Car le Filtre pourrait filtrer des requ�tes autres que HTTP
		HttpServletRequest request = (HttpServletRequest) req;
        HttpServletResponse response = (HttpServletResponse) res;
		
        //R�cup�ration de session
		HttpSession session = request.getSession();
		
		if ( session.getAttribute( "pseudo" ) == null ) {
			//Si non connect�, on retourne vers la page publique
			response.sendRedirect( request.getContextPath() + "/refus.jsp" );
		} else {
			//Si connect�, notre filtre est pass�
			chain.doFilter( request, response );
		}
	}

	/**
	 * @see Filter#init(FilterConfig)
	 */
	public void init(FilterConfig fConfig) throws ServletException {
		// TODO Auto-generated method stub
	}

}
