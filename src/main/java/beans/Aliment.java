/*import java.util.List;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;
*/

package beans;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.*;

@Entity
@Table(name = "ALIMENT")

public class Aliment 
{
	@Id @GeneratedValue(strategy = GenerationType.IDENTITY)
		
	@Column(name="id")
	private int id;
	
	@Column(name="nom")
	private String nom;
	
	@Column(name="cat")
	private String cat;
	
	@ManyToMany(mappedBy = "listAlimRec")
	private List<Recette> listRecAlim = new ArrayList<Recette>();
	
	public Aliment() {}	
	
	
	
	public void addRec (Recette r)
	{
		listRecAlim.add(r);
	}
	
	
	
	public List<Recette> getListRecAlim() {return listRecAlim;}
	public void setListRecAlim(List<Recette> listRecAlim) {this.listRecAlim = listRecAlim;}
	public String getNom() {return nom;}
	public String getCat() {return cat;}
	public void setNom(String nom) {this.nom = nom;}
	public void setCat(String type) {this.cat = type;}
	
}

	//public String getNom() {return nom;}
	//public void setNom(String nom) {this.nom = nom;}
	//public String getType() {return type;}
	//public void setType(String type) {this.type = type;}
	//private String nom;
	//private String type;	
	//connexion SQL
	/*try {Class.forName("com.mysql.jdbc.Driver");}
	catch(ClassNotFoundException e) {System.out.println("Erreur lors du chargement : le driver n'a pas ete trouve dans le classpath!" + e.getMessage());}
	String url="jdbc:mysql://localhost:3342/mydata";
	String utilisateur ="root";
	String motDePasse="pass";
	Connection connexion=null;
	
	try {	connexion = DriverManager.getConnection(url, utilisateur, motDePasse);
			Statement statement = connexion.createStatement();
			connexion.close();
		}
	catch (SQLException e) {System.out.println("Erreur lors de la connexion ou de l'execution de la requete:" + e.getMessage());}
	*/
