package beans;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.*;

@Entity
public class Recette 
{
	@Id @GeneratedValue(strategy = GenerationType.IDENTITY)
	
	@Column(name="clef")
	private int clef;
	
	@Column(name="nomrec")
	private String nomrec;
	
	//@ManyToMany (cascade = {CascadeType.PERSIST, CascadeType.MERGE})
	@ManyToMany(cascade = { CascadeType.ALL })
    @JoinTable(
        name = "Recette_Aliment", 
        joinColumns = { @JoinColumn(name = "idrecette") }, 
        inverseJoinColumns = { @JoinColumn(name = "idaliment") }
    )
	private List<Aliment> listAlimRec = new ArrayList<Aliment>();
	
	public Recette() {}

	//List<Aliment> alim = new ArrayList<Aliment> ();
	
	public void addAliment(Aliment a)
	{
		listAlimRec.add(a);
		//a.getListRecAlim().add(this);
		a.addRec(this);
	}
	
	
	
	public List<Aliment> getListAlimRec() {return listAlimRec;}
	public void setListAlimRec(List<Aliment> listAlimRec) {this.listAlimRec = listAlimRec;}
	//public List<Aliment> getAlim() {return alim;}
	//public void setAlim(List<Aliment> alim) {this.alim = alim;}
	public int getClef() {return clef;}
	public void setClef(int clef) {this.clef = clef;}
	public String getNomrec() {return nomrec;}
	public void setNomrec(String nomrec) {this.nomrec = nomrec;}
	
}
