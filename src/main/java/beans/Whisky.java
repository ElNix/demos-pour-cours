package beans;

import javax.persistence.Column;
import javax.persistence.Entity;

@Entity
public class Whisky extends Boisson {

	@Column
	String type;

	public Whisky(String nom, String type) {
		super(nom);
		this.type = type;
	}

	public Whisky() {
		// TODO Auto-generated constructor stub
	}

	public String gettype() {
		return type;
	}

	public void settype(String type) {
		this.type = type;
	}
	
}
