	
	package beans;
	
	public class Canape {
	
		private int nbPlaces;
		private String motif;
		private String couleur;
	
		public String descriptionCanape()
		{
			return "Canape de "+nbPlaces+" places au magnifique motif "+motif+" de couleur "+couleur;
		}
	


		//Getters Setters
		//Surcharges de constructeurs optionnelles
	
		public Canape() {
			super();
		}
		
		@Override
		public String toString() {
			return "Canape [nbPlaces=" + nbPlaces + ", motif=" + motif + ", couleur=" + couleur + "]";
		}



		public Canape(int nbPlaces, String motif, String couleur) {
			super();
			this.nbPlaces = nbPlaces;
			this.motif = motif;
			this.couleur = couleur;
		}

		public int getNbPlaces() {
			return nbPlaces;
		}
	
		public void setNbPlaces(int nbPlaces) {
			this.nbPlaces = nbPlaces;
		}
	
		public String getMotif() {
			return motif;
		}
	
		public void setMotif(String motif) {
			this.motif = motif;
		}
	
		public String getCouleur() {
			return couleur;
		}
	
		public void setCouleur(String couleur) {
			this.couleur = couleur;
		}
	
	
	
	}
