package beans;

import javax.persistence.*;

@Entity
public class Biere extends Boisson {
	
	@Column
	String couleur;

	public Biere(String nom, String couleur) {
		super(nom);
		this.couleur = couleur;
	}

	public Biere() {
		// TODO Auto-generated constructor stub
	}

	public String getCouleur() {
		return couleur;
	}

	public void setCouleur(String couleur) {
		this.couleur = couleur;
	}
	
	

}
