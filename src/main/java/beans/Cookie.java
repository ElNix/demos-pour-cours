package beans;

public class Cookie {
	
	private int diametre;
	private String farine;
	private String topping;
	
	public Cookie() {
		super();
	}

	public Cookie(int diametre, String farine, String topping) {
		super();
		this.diametre = diametre;
		this.farine = farine;
		this.topping = topping;
	}

	@Override
	public String toString() {
		return "Cookie de " + diametre + "cm de diametre à la farine " + farine + " et un topping " + topping + ".";
	}
	
	public int getDiametre() {
		return diametre;
	}
	public void setDiametre(int diametre) {
		this.diametre = diametre;
	}
	public String getFarine() {
		return farine;
	}
	public void setFarine(String farine) {
		this.farine = farine;
	}
	public String getTopping() {
		return topping;
	}
	public void setTopping(String topping) {
		this.topping = topping;
	}
	
	
	

}
