package beans;

import javax.persistence.*;

@Entity
public class Boisson {
	
	@Id
	@GeneratedValue
	protected int id;
	
	@Column
	protected String nom;

	public Boisson(String nom) {
		super();
		this.nom = nom;
	}

	public Boisson() {
		super();
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}
	
	

}
