package beans;

import javax.persistence.*;

@Entity
@Table(name = "USERS")
public class User {
	
	@Id @GeneratedValue(strategy = GenerationType.IDENTITY)
	private int ID;
	private String pseudo;
	private String email;
	private int points;
	
	public User() {
		super();
	}
	
	public User(String pseudo, String email) {
		super();
		this.pseudo = pseudo;
		this.email = email;
		this.points = 0;
	}
	
	public int getID() {
		return ID;
	}
	public void setID(int iD) {
		ID = iD;
	}
	public String getPseudo() {
		return pseudo;
	}
	public void setPseudo(String pseudo) {
		this.pseudo = pseudo;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public int getPoints() {
		return points;
	}
	public void setPoints(int points) {
		this.points = points;
	}
	
	@Override
	public String toString() {
		return "User [ID=" + ID + ", pseudo=" + pseudo + ", email=" + email + ", points=" + points + "]";
	}

}
