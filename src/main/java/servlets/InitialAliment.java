package servlets;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import beans.Aliment;
import beans.Recette;

public class InitialAliment extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    
    public InitialAliment() {super();}

	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		
		Aliment aubg = new Aliment();
		aubg.setNom("aubergine");
		aubg.setCat("fruit/l&eacutegume");
		
		Aliment moule = new Aliment();
		moule.setNom("moule");
		moule.setCat("viande/poisson");
		
		Aliment biere = new Aliment ();
		biere.setNom("bi&egravere");
		biere.setCat("divers");
		
		Aliment pain = new Aliment ();
		pain.setNom("pain");
		pain.setCat("f&eacuteculent");
		
		Aliment jambon = new Aliment ();
		jambon.setNom("jambon");
		jambon.setCat("viande/poisson");
		
		Aliment moutarde = new Aliment();
		moutarde.setNom("moutarde");
		moutarde.setCat("divers");
		
		Aliment poivron = new Aliment();
		poivron.setNom("poivron");
		poivron.setCat("fruit/l&eacutegume");
		
		Aliment plasagne = new Aliment();
		plasagne.setNom("lasagnes");
		plasagne.setCat("f&eacuteculent");
		
		Aliment beurre = new Aliment();
		beurre.setNom("beurre");
		beurre.setCat("divers");
		
		Aliment lait = new Aliment();
		lait.setNom("lait");
		lait.setCat("divers");
		
		Aliment huile = new Aliment();
		huile.setNom("huile");
		huile.setCat("divers");
		
		Aliment coug = new Aliment();
		coug.setNom("courgette");
		coug.setCat("fruit/l&eacutegume");
		
		Aliment tom = new Aliment();
		tom.setNom("tomate");
		tom.setCat("fruit/l&eacutegume");
		
		Aliment pom = new Aliment();
		pom.setNom("pomme");
		pom.setCat("fruit/l&eacutegume");
		
		Aliment pat = new Aliment();
		pat.setNom("p&acircte");
		pat.setCat("f&eacuteculent");
		
		Aliment bo = new Aliment ();
		bo.setNom("boeuf");
		bo.setCat("viande/poisson");
		
		Aliment lard = new Aliment ();
		lard.setNom("lardon");
		lard.setCat("viande/poisson");
		
		Aliment moul = new Aliment ();
		moul.setNom("moule");
		moul.setCat("viande/poisson");
		
		Aliment choc = new Aliment();
		choc.setNom("chocolat");
		choc.setCat("divers");
		
		Aliment suc = new Aliment();
		suc.setNom("sucre");
		suc.setCat("divers");
		
		Aliment far = new Aliment();
		far.setNom("farine");
		far.setCat("divers");
		
		Aliment of = new Aliment ();
		of.setNom("oeuf");
		of.setCat("viande/poisson");
		
		Aliment crem = new Aliment();
		crem.setNom("cr&egraveme fra&icircche");
		crem.setCat("divers");
		
		Aliment from = new Aliment();
		from.setNom("fromage");
		from.setCat("divers");
		
		Aliment sel = new Aliment();
		sel.setNom("sel");
		sel.setCat("divers");
		
		Aliment poi = new Aliment();
		poi.setNom("poivre");
		poi.setCat("divers");
		
		SessionFactory factory = new Configuration().configure().buildSessionFactory();
		Session session = factory.openSession();	
		session.beginTransaction();
		session.createQuery("delete FROM beans.Recette").executeUpdate();
		session.createQuery("delete FROM beans.Aliment").executeUpdate();
		
		
		session.save(poi);
		session.save(sel);
		session.save(from);
		session.save(crem);
		session.save(of);
		session.save(far);
		session.save(suc);
		session.save(choc);
		session.save(aubg);
		session.save(tom);
		session.save(pom);
		session.save(pat);
		session.save(bo);
		session.save(lard);
		session.save(moul);
		session.save(huile);
		session.save(lait);
		session.save(beurre);
		session.save(plasagne);
		session.save(poivron);
		session.save(pain);
		session.save(jambon);
		session.save(biere);
		session.save(moutarde);
		

		
		Recette patecarbo = new Recette();
		patecarbo.setNomrec("P�te carbonara");
		patecarbo.addAliment(poi);
		patecarbo.addAliment(sel);
		patecarbo.addAliment(from);
		patecarbo.addAliment(crem);
		patecarbo.addAliment(of);
		patecarbo.addAliment(pat);
		patecarbo.addAliment(lard);
		
		Recette wel = new Recette();
		wel.setNomrec("Welsh");
		wel.addAliment(from);
		wel.addAliment(of);
		wel.addAliment(sel);
		wel.addAliment(moutarde);
		wel.addAliment(jambon);
		wel.addAliment(pain);
		wel.addAliment(biere);
		wel.addAliment(poi);
		
		
		session.save(wel);
		session.save(patecarbo);
		
		session.getTransaction().commit();
		session.close();
		
		response.getWriter().append("Served at: ").append(request.getContextPath()); 
	}

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		doGet(request, response);
	}

}
