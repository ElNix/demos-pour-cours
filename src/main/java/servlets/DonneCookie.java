package servlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.Cookie;

/**
 * Servlet implementation class DonneCookie
 */
public class DonneCookie extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public DonneCookie() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String commande = request.getParameter("commande");
		
		Cookie cookie;
		
		if(commande.equals("banane"))
			cookie = new Cookie(10,"blanche","morceaux de banane");
		else if(commande.equals("doublechoco"))
			cookie = new Cookie(15,"au chocolat noir","pépites de chocolat");
		else if(commande.equals("fraise"))
			cookie = new Cookie(8,"blanche","morceaux de fraise");
		else
			cookie = new Cookie(0,"ERREUR","ERREUR");
		
		request.setAttribute( "cookie", cookie );
		this.getServletContext().getRequestDispatcher( "/WEB-INF/donneCookie.jsp" ).forward( request, response );
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
