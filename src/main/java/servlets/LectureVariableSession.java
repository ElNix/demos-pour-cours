package servlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class LectureVariableSession
 */
public class LectureVariableSession extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public LectureVariableSession() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		
		//Activation de la session
		//La cr�e si c'est la premi�re fois, la r�cup�re si elle existe d�j�
		HttpSession session = request.getSession();
		
		//Si on en a besoin
		//R�cup�ration de la variable de session
		String ex = (String) session.getAttribute("exemple");
		System.out.println("Valeur de la variable de session \"exemple\" : "+ex);
		
		//Ici, nous voulons juste l'afficher
		//Pas besoin de transmettre la variable � la JSP !
		//Les JSP peuvent directement r�cup�rer les variables de session !

		this.getServletContext().getRequestDispatcher( "/WEB-INF/lectureVariableSession.jsp" ).forward( request, response );
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
