package servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import beans.Biere;
import beans.Boisson;
import beans.Whisky;

/**
 * Servlet implementation class EnregistreBoisson
 */
public class EnregistreBoisson extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public EnregistreBoisson() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Configuration config = new Configuration();
		SessionFactory sessionFactory = config.configure().buildSessionFactory();
		Session session = sessionFactory.openSession();
		session.beginTransaction();

		/*
        Boisson b1 = new Biere("La Bete","blonde");
        Boisson b2 = new Biere("Castel Red","rouge");
        Boisson w1 = new Whisky("Nikka","fruite");
        Boisson w2 = new Whisky("Caol Ila","tourbe");
        
        session.save(b1);
        session.save(b2);
        session.save(w1);
        session.save(w2);
        */
		
		Biere b1 = new Biere("Delirium Black","black");
		session.save(b1);

		session.getTransaction().commit();
		session.close();
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
