package servlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class AffichageDonnees
 */

	public class AffichageDonnees extends HttpServlet {
		private static final long serialVersionUID = 1L;
	
		protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
			//Pr�paration des donn�es
			String donnee1 = "Bonjour, je suis du texte !";
			int donnee2 = 42;
			
			//Ajout des donn�es dans la requ�te
			//1er argument : le nom que va avoir la donn�e dans la JSP
			//2e argument : la donn�e elle m�me
			request.setAttribute("texte", donnee1);
			request.setAttribute("nombre", donnee2);
			
			//On indique quelle JSP appeler pour lui fournir les donn�es
			//La seule chose � changer est le string indiquant le chemin de la JSP
			//this.getServletContext().getRequestDispatcher( "/WEB-INF/affichage.jsp" ).forward( request, response );
			this.getServletContext().getRequestDispatcher( "/WEB-INF/affichageEL.jsp" ).forward( request, response );
		}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
