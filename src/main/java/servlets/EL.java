package servlets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.Canape;

/**
 * Servlet implementation class EL
 */
@WebServlet( name="EL", urlPatterns = "/el" )
public class EL extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public EL() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		int valeur = 42;
		String code = "alert('yo');";
		
		request.setAttribute("valeur", valeur);
		request.setAttribute("code", code);
		
		
		
		String meteo = "Soleil";
		
		request.setAttribute("meteo", meteo);
		
		List<Canape> liste = new ArrayList<Canape>();
		liste.add(new Canape(1, "Fleurs", "rose"));
		liste.add(new Canape(2, "Poissons", "bleu"));
		liste.add(new Canape(3, "Canards", "vert"));
		
		request.setAttribute("liste", liste);
		
		
		this.getServletContext().getRequestDispatcher( "/WEB-INF/el.jsp" ).forward( request, response );
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
