package servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class AfficheCookies
 */
public class AfficheCookies extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public AfficheCookies() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	
	
	//M�thode personnalis�e de lecture de cookies
	//Car nous sommes condamn�s � parcourir l'int�gralit� des cookies
	//Pour trouver celui qui nous interesse 
	private static String getCookieValue( HttpServletRequest request, String nom ) {
		Cookie[] cookies = request.getCookies();
		if ( cookies != null ) {
			for ( Cookie cookie : cookies ) {
				if ( cookie != null && nom.equals( cookie.getName() ) ) {
					return cookie.getValue();
				}
			}
		}
		return null;
	}
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) 
			throws ServletException, IOException {

		request.setAttribute("cookies", request.getCookies());

		//Affichage d'un cookie � partir de notre m�thode personnalis�e
		System.out.println(getCookieValue(request, "valeur"));

		this.getServletContext().getRequestDispatcher( "/WEB-INF/affichageCookies.jsp" ).forward( request, response );
	}


	

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
