package servlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class Addition
 */
@WebServlet(urlPatterns="/Addition")
public class AdditionPourAJAX extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public AdditionPourAJAX() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String nb1 = request.getParameter("nb1");
		String nb2 = request.getParameter("nb2");

		//VERSION IF
		if(nb1 == null || nb2 == null)
		{
			response.getWriter().append("Veuillez me donner nb1 et nb2 s'il vous plait");
		}
		else if (isNumeric(nb1) && isNumeric(nb2)) {
			int resultat = Integer.parseInt(nb1)+Integer.parseInt(nb2);
			response.getWriter().append(""+resultat);
		} else {
			response.getWriter().append("Veuillez me donner 2 nombres s'il vous plait");
		}
		
		//VERSION TRY/CATCH
		/*
		try {
			if (isNumeric(nb1) && isNumeric(nb2)) {
				int resultat = Integer.parseInt(nb1)+Integer.parseInt(nb2);
				response.getWriter().append(""+resultat);
			} else {
				response.getWriter().append("Veuillez me donner 2 nombres s'il vous plait");
			}
		} catch (NullPointerException e) {
			response.getWriter().append("Veuillez me donner nb1 et nb2 s'il vous plait");
		}
		*/
	}
	
	public static boolean isNumeric(String str) {
		  return str.matches("-?\\d+(\\.\\d+)?");  //match a number with optional '-' and decimal.
		}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
