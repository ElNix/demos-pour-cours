package servlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class CreationCookie
 */
public class CreationCookie extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public CreationCookie() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
    
    //M�thode personnalis�e de cr�ation de cookie
    //Simplifie bien la vie
	private static void setCookie( HttpServletResponse response, String nom, String valeur, int maxAge ) {
		//Instanciation du cookie avec le nom et la valeur de la variable
	    Cookie cookie = new Cookie( nom, valeur );
	    //D�finition de la dur�e de stockage du cookie
	    cookie.setMaxAge( maxAge );
	    //Mise du cookie dans le navigateur
	    response.addCookie( cookie );
	}
    
	protected void doGet(HttpServletRequest request, HttpServletResponse response) 
			throws ServletException, IOException {
		
		//Dur�e de vie maximum d'un cookie en secondes
		int COOKIE_MAX_AGE = 60 * 60 * 24 * 365;  // 1 an

		//Appel � notre m�thode personnalis�e
		setCookie( response, "valeur", "quaranteDeux", COOKIE_MAX_AGE );
		
		//Pour supprimer
		//setCookie( response, "valeur", "", 0 );
		
		response.sendRedirect("AfficheCookies");
	}


	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
